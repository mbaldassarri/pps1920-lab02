package u02.solutions

import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._
import u02.solutions.Task4.Shape
import u02.solutions.Task4.Shape.{Circle, Rectangle, Square}

class ShapeTest {
  val rectangle:Shape = Rectangle(3,4)
  val circle:Shape = Circle(4)
  val square:Shape = Square(4)

  @Test def testPerimeter()= {
    assertEquals(14, Task4.Shape.perimeter(rectangle))
    assertEquals(25.12, Task4.Shape.perimeter(circle))
    assertEquals(16, Task4.Shape.perimeter(square))
  }

  @Test def testArea() = {
    assertEquals(12, Task4.Shape.area(rectangle))
    assertEquals(50.24, Task4.Shape.area(circle))
    assertEquals(16, Task4.Shape.area(square))
  }
}

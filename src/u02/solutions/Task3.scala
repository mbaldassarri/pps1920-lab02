package u02.solutions

import u02.Values.n
import u02.solutions.Task3.fib

import scala.annotation.tailrec

object Task3 extends App{

  def factorial(n: Int): Int = n match {
    case 0 | 1 => 1   // 0 or 1 gets mapped to 1
    case _ => n * factorial(n - 1)  // otherwise
  }
  println(factorial(6)) // 720

  def fib(n: Int): Int = n match {
    case 0 | 1 => n
    case _ => fib(n - 1) + fib(n - 2)
  }
  println(fib(9))

  //tail recursion version
  def fibonacciTail(x:Int):Int = {
    @tailrec def fibTail(n:Int, y:Int, z: Int):Int = n match {
      case 0 => y
      case 1 => z
      case _ => fibTail(n - 1, y, y + z)
    }
    fibTail(x, 0, 1)
  }
  println(fibonacciTail(9))

}

package u02.solutions

object Task5 extends App{

    sealed trait Option[A] // An Optional data type
    object Option {
      case class None[A]() extends Option[A]
      case class Some[A](a: A) extends Option[A]

      def isEmpty[A](opt: Option[A]): Boolean = opt match {
        case None() => true
        case _ => false
      }

      def getOrElse[A, B >: A](opt: Option[A], orElse: B): B = opt match {
        case Some(a) => a
        case _ => orElse
      }

      def flatMap[A,B](opt: Option[A])(f:A => Option[B]): Option[B] = opt match {
        case Some(a) => f(a)
        case _ => None()
      }

      // Task 5 - more functional combinators
      def filter[A](option: Option[A])(pred: A => Boolean): Any = option match {
        case Some(a) => if (pred(a)) Some(a) else None()
        case None() => None()
      }

      def map[A,B](option: Option[A])(pred: A => B):Option[B] = option match {
        case Some(a) => Some(pred(a))
        case _ => None()
      }

      def map2[A,B,C](option1:Option[A])(option2: Option[B])(pred: (A, B) => C): Any = (option1, option2) match {
        case (Some(a), Some(b)) => pred(a, b)
        case _ => None()
      }
    }

  import Option._
  val s1: Option[Int] = Some(1)
  val s2: Option[Int] = Some(2)
  val s3: Option[Int] = None()

  println("filter: " + filter(Some(5))(_ > 2)) // Some(5)
  println("filter: " + filter(Some(5))(_ > 8)) // None()

  println("map: " + map(Some(5))(_ > 2)) // Some(true)
  println("map: " + map(Some(5))(_ > 8)) // Some(false)
  println("map: " + map(None[Int])(_ > 2)) // None()

  println("map2: " + map2(Some(5))(Some(3))(_ * _)) // 15
  println("map2: " + map2(Some(5))(Some(3))(_ - _)) // 2

  println("------------------------")
  println(s1) // Some(1)
  println(getOrElse(s1,0), getOrElse(s3,0)) // 1,0
  println(getOrElse(s3,None()))
  println(flatMap(s1)(i => Some(i+1))) // Some(2)
  println(flatMap(s1)(i => flatMap(s2)(j => Some(i+j)))) // Some(3)
  println(flatMap(s1)(i => flatMap(s3)(j => Some(i+j)))) // None
}

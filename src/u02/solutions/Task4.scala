package u02.solutions

object Task4 extends App{
  sealed trait Shape
  object Shape {
    case class Rectangle(x:Double, y:Double) extends Shape
    case class Circle(radius:Double) extends Shape
    case class Square(z:Double) extends Shape

    //3.14 can be replaced with Math.PI
    def perimeter(shape: Shape): Double = shape match {
      case Rectangle(x, y) => (x + y) * 2
      case Circle(radius) => radius * 3.14 * 2
      case Square(z) => z * 4
    }

    def area(shape: Shape): Double = shape match {
      case Rectangle(x, y) => x * y
      case Circle(radius) => 3.14 * radius * radius
      case Square(z) => z * z
    }
  }
}

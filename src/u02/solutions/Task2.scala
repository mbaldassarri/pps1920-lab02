package u02.solutions

object Task2 extends App {
  //Task part 2a a)
  val parityVal: Int => String = {
    case x if x % 2 == 0 => "even"
    case _ => "odd"
  }

  def parityDef(x: Int): String = x % 2 match {
    case 0 => "even"
    case _ => "odd"
  }

  println(parityVal(2))
  println(parityVal(3))
  println(parityVal(2))
  println(parityVal(3))

  //Task part 2a b)
  println("------------------------")
  val negVal: (String => Boolean) => (String => Boolean) = pred => !pred(_)

  def negDef(pred: String => Boolean): String => Boolean = !pred(_)

  def negGeneric[T](pred: T => Boolean): T => Boolean = !pred(_)

  val empty: String => Boolean = _ == "" // predicate on strings
  val notEmptyVal = negVal(empty)
  val notEmptyDef = negDef(empty)
  val notEmptyDefGeneric = negGeneric(empty)

  println(notEmptyDef("foo")) //true
  println(notEmptyDef("")) //false
  println(notEmptyVal("foo")) //true
  println(notEmptyVal("")) //false
  println(notEmptyVal("foo") && !notEmptyVal("")) //true
  println(notEmptyDefGeneric("foo")) //true
  println(notEmptyDefGeneric("")) //false

  //Task part 2b - curriying
  println("------------------------")
  val checkVal: (Int, Int, Int) => Boolean = (x, y, z) => x <= y && y <= z

  def checkDef(x: Int, y: Int, z: Int): Boolean = x <= y && y <= z

  val checkCurriedVal: Int => Int => Int => Boolean = x => y => z => x <= y && y <= z

  def checkCurriedDef(x: Int)(y: Int)(z: Int): Boolean = x <= y && y <= z

  println(checkVal(1, 2, 3)) //true
  println(checkDef(3, 2, 1)) //false
  println(checkCurriedVal(1)(2)(2)) //true
  println(checkCurriedDef(1)(2)(1)) //false

  //Task part 2b - functional composition
  println("------------------------")

  def compose(f: Int => Int, g: Int => Int): Int => Int = x => f(g(x))

  def composeGeneric[A, B, C](f: B => C, g: A => B): A => C = x => f(g(x))
  //or
  def composeGenericInverse[A, B, C](f: A => B, g: B => C): A => C = x => g(f(x))

  println(compose(_ - 1, _ * 2)(5)) // 9
  println(composeGeneric[Int, Int, Int](_ - 1, _ * 2)(5)) // 9
  println(composeGenericInverse[String, String, String](_ + " ", _ + "world")("hello")) // hello world


}
